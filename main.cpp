#include <iostream>

using namespace std;

void d()
{
    cout <<"|\\    " << endl;
    cout <<"| \\   " << endl;
    cout <<"|  \\  " << endl;
    cout <<"|  /  " << endl;
    cout <<"| /   " << endl;
    cout <<"|/    " << endl;
}

void a()
{/*
    cout << "    /|" << endl;
    cout << "   / |" << endl;
    cout << "  /  |" << endl;
    cout << " /---|" << endl;
    cout << "/    |" << endl;
    cout << "|    |" << endl;*/
    cout << "      " << endl;
    cout << "  /\\  " << endl;
    cout << " /__\\ " << endl;
    cout << "/    \\" << endl;
    cout << "      " << endl;
    cout << "      " << endl;
}

void n()
{
    cout <<"|\\    |"<<endl;
    cout <<"| \\   |"<<endl;
    cout <<"|  \\  |"<<endl;
    cout <<"|   \\ |"<<endl;
    cout <<"|    \\|"<<endl;
    cout <<"|     \\"<<endl;

}

void y()
{
    cout <<"\\    /"<<endl;
    cout <<" \\  / "<<endl;
    cout <<"  \\/  "<<endl;
    cout <<"  /   "<<endl;
    cout <<" /    "<<endl;
    cout <<"/     "<<endl;
}

int main()
{
    d();
    a();
    n();
    y();
    return 0;
}
